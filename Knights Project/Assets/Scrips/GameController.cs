﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	private int turretCount;
	public int GetTurretCount() { return turretCount;}
	public void IncreaseTurretCount() { turretCount++;
	}
	public void DecreaseTurretCount() {
		turretCount--;
	}
	private bool wake;
	public bool ShouldWake() { return wake;}
	public bool CanMakeTowers() {
		return !wake;
	}
	// Use this for initialization
	void Start () {
		turretCount = 0;
		wake = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (turretCount >= 100) {
			wake=true;
		}
	}
}
