﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour {

	public GameObject shot;
	public Transform spawnShotLocation;

	private GameController gc;
	private SpriteRenderer sr;
	private bool pastWaking;

	// Use this for initialization
	void Start () {
		StartCoroutine ("startRotating");
		sr = GetComponent<SpriteRenderer> ();
		gc = FindObjectOfType<GameController> ();
		pastWaking = false;
	}
	
	// Update is called once per frame
	void Update () {
		 if (gc.ShouldWake () && !pastWaking) {
			pastWaking=true;
			StopCoroutine("startRotating");
			StartCoroutine("startRotating");
		}
	}

	IEnumerator startRotating()
	{
		for(int i=0;i<12;i++)
		{
			transform.Rotate(0.0f,  0.0f,Random.Range(15.0f,45.0f));
			Shoot ();
			yield return new WaitForSeconds(0.5f);
		}
		sr.color = new Color (255, 255, 255);

	}

	void Shoot() {
		Instantiate (shot, 
		             spawnShotLocation.position, 
		             transform.rotation);
	}
}
