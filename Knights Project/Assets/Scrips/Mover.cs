﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour {


	public GameObject spawn;
	public float speed;

	private Rigidbody2D rb;
	private Transform tra;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
		tra = GetComponent<Transform> ();
		StartCoroutine ("boom");
	}
	// Update is called once per frame
	void Update () {
		//nie pracowałem niigdy przy 2d więc nie udało mi sie w szybkim czasie znaleźć rozwiązania

		transform.Translate((transform.up) * speed * 10 * Time.deltaTime);


		//float angle = tra.eulerAngles.magnitude * Mathf.Deg2Rad;
		//Debug.Log (tra.position.ToString ());
		//Debug.Log (tra.forward.ToString());
		//Vector3 nextPos = new Vector3 ();
		//nextPos = tra.position;
		//nextPos.x += (Mathf.Cos (angle) * speed) * Time.deltaTime;
		//nextPos.y += (Mathf.Sin (angle) * speed) * Time.deltaTime;
		//nextPos.z = 0.0f;

		//tra.position = nextPos;
			//new Vector3 (tra.position.x + speed*Time.deltaTime, tra.position.y + speed*Time.deltaTime, 0.0f);
		//rb.velocity = transform.forward * speed;
	}
	
	IEnumerator boom()
	{
		yield return new WaitForSeconds (Random.Range (0.25f,1.0f));
		
		var gc = FindObjectOfType<GameController> ();

		if (gc.CanMakeTowers()) {
			var turret = Instantiate (spawn, tra.position, Quaternion.identity) as GameObject;
			gc.IncreaseTurretCount ();
		}

		Destroy (this.gameObject);

	}
}
